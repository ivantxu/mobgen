const races = [
    {
        "name":"CARRERA POPULAR NOVO MESOIRO",
        "id":"0",
        "photo":"https://championchipnorte.com/uploads/images/cartel-565bfd323ea5e0.37643322.jpg",
        "length": 5535,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"CARRERA POPULAR LOS ROSALES",
        "id":"1",
        "photo":"https://championchipnorte.com/uploads/images/cartel-564f25bb114180.86511118.png",
        "length": 6465,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"CARRERA POPULAR MATOGRANDE",
        "id":"2",
        "photo":"http://4.bp.blogspot.com/-emOQq_WptOU/VoUAGB1TDcI/AAAAAAAAJKI/IBtSJSP21uQ/s1600/Cartel.png",
        "length": 6625,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"CARRERA TORRE DE HÉRCULES",
        "id":"3",
        "photo":"https://championchipnorte.com/uploads/images/cartel-56c705ed703b07.55917798.jpg",
        "length": 6590,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"BAIXADA POPULAR DE CROSS SAN PEDRO DE VISMA",
        "id":"4",
        "photo":"https://championchipnorte.com/uploads/images/cartel-575568e6dcdc47.44013992.jpg",
        "length": 6490,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"CARRERA POPULAR VOLTA A OZA",
        "id":"5",
        "photo":"https://championchipnorte.com/uploads/images/cartel-578df5723e2964.90506325.jpg",
        "length": 7210,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    },{
        "name":"CARRERA POPULAR VENTORRILLO",
        "id":"6",
        "photo":"https://championchipnorte.com/uploads/images/cartel-57b348153a86a9.55534591.jpg",
        "length": 6820,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    }
]

export default races;