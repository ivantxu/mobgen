const results = {
    "mobgen": {
        "0": {

        },
        "1": {
            "2016": {
                "time": "37:28",
                "position": "78"
            },
            "2017": {
                "time": "26:33",
                "position": "23"
            }
        },
        "2": {
            "2016": {
                "time": "45:23",
                "position": "87"
            },
            "2017": {
                "time": "24:23",
                "position": "45"
            }
        },
        "3": {
            "2016": {
                "time": "29:29",
                "position": "23"
            },
            "2017": {
                "time": "34:12",
                "position": "64"
            }
        },
        "4": {
            "2016": {
                "time": "46:28",
                "position": "46"
            },
            "2017": {
                "time": "35:46",
                "position": "84"
            }
        },
        "5": {
            "2016": {
                "time": "28:18",
                "position": "32"
            },
            "2017": {
                "time": "19:57",
                "position": "64"
            }
        },
        "6": {
            "2016": {
                "time": "43:53",
                "position": "95"
            },
            "2017": {
                "time": "24:52",
                "position": "34"
            }
        },



    }
}

export default results;