import React from 'react';
import {Table} from 'react-bootstrap'

/*
* A table displaying all of the user's results
* */
const ResultTable = React.createClass({
    render() {
        let resultArray = typeof this.props.results[this.props.user.loggedUser] !== 'undefined' ?
            this.props.results[this.props.user.loggedUser][this.props.raceId] :
            {};
        /*
        * Renders a line of the table
        * */
        const renderResultItem = (result, index) => {
            return(<tr key={index}>
                <td>{result}</td>
                <td>{resultArray[result].position}</td>
                <td>{resultArray[result].time}</td>
            </tr>);
        };
        return (
            <div>
                { Object.keys(resultArray).length>0 &&
                    <Table striped bordered condensed hover className='result-table'>
                        <thead>
                        <tr>
                            <th>Year</th>
                            <th>Position</th>
                            <th>Mark</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(resultArray).map(renderResultItem)}
                        </tbody>
                    </Table>
                }
            </div>
        )
    }
});

export default ResultTable;
