import React from 'react';
import { Link } from 'react-router';
import Login from './Login'
import RaceGrid from './RaceGrid'

const Home = React.createClass({
    /*
    * By default we show the login form to avoid user data exposure
    * */
    getInitialState() {
        return {
            showLogin: true
        };
    },
    /*
    * When the component mounts we check if there is a user logged to show the grid or the login form
    * */
    componentDidMount() {
        this.setState({showLogin: typeof this.props.user.loggedUser === 'undefined'})
    }
    ,
    /*
    * Callback function to pass to the login component, when executed the login form will be hidden
    * It should be executed when there is a successful login.
    * */
    showContent() {
        this.setState({showLogin: false});
    },
    /*
    * The home component has the login form and the grid with all the races listed
    * */
    render() {
        /*
        * Either the login or race grid component will be shown in the root path, based on login status
        * */
        let whichComponent = this.state.showLogin ?
            <Login {...this.props} toggleLogin={this.showContent}/> :
            <RaceGrid {...this.props}/>;
        return (
            <div className='container'>
                {whichComponent}
            </div>
        )
    }
});

export default Home;
