import React from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form'
import { Button,FormGroup, ControlLabel, FormControl } from 'react-bootstrap'
/*
 *  Creates a custom field for Redux Form with validation
 * */
const renderField = ({ type, label, input, meta: { touched, error } }) => (
    <div className='input-row'>
        <FormGroup validationState={getValidationState(error)}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl type={type} { ...input } />
        </FormGroup>
        {touched && error &&
        <div className='error'><span>{error}</span></div>}
    </div>
);

/*
 * Set the validation error to change React Bootstrap field color
 * */
const getValidationState = (errors) => {
    return errors ? 'error' : null;
};

/*
* The login form has validation for required fields and also displays
* a message if the validation process hasn't succeeded
* */
let Login = React.createClass({
    /*
    * Initializing the state of the authentication process
    * */
    getInitialState() {
        return {
            authenticationFailed: false
        };
    },
    /*
    * Validate the login form for required fields.
    * If the fields validate we check if the login/password combination is correct.
    * If the credentials are OK we add the logged user to the store.
    * */
    submit (values, dispatch, props) {
        const { login = '', password = ''} = values;
        let error = {};
        let isError = false;
        if (login.trim() === '') {
            error.login = 'Required';
            isError = true;
        }
        if (password.trim() === '') {
            error.password = 'Required';
            isError = true;
        }
        if (isError) {
            throw new SubmissionError(error)
        } else {
            if(values.login === props.user.login && values.password === props.user.password){
                dispatch(props.doLogin(values.login));
                this.props.toggleLogin();
                this.authenticationState(false)
            } else {
                error.badCredentials = 'Authentication failure';
                this.authenticationState(true)
            }
        }


    },
    /*
    * Sets or disables the failed login message
    * */
    authenticationState (authenticationProblem) {
        this.setState({authenticationFailed : authenticationProblem})
    },
    render() {
        const { handleSubmit } = this.props;
        this.authenticationFailed = false;
        return (
            <div className='col-sm-push-4 col-sm-5'>
                <form onSubmit={handleSubmit(this.submit)}>
                    <Field name='login' label='USER' component={renderField} type='text' />
                    <Field name='password' label='PASSWORD' component={renderField} type='password' />
                    {this.state.authenticationFailed && <p>Authentication failed</p>}
                    <Button
                        className='submit-button'
                        bsStyle='primary'
                        bsSize='small'
                        type='submit'
                    >
                        SUBMIT
                    </Button>
                </form>
            </div>
        )
    }
});

Login = reduxForm({
    form: 'login'
})(Login);
export default Login;