import React from 'react';
import ResultTable from './ResultTable'
import AddResult from './AddResult'

/*
* Shows the result table and the button to trigger the add result modal
* */
const Results = React.createClass({
    render() {
        return (
            <div>
                <ResultTable {...this.props}/>
                <AddResult {...this.props}/>
            </div>
        )
    }
});

export default Results;
