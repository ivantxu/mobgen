import React from 'react';
import { Link } from 'react-router';

/*
* Contains a common template for all the views and sets where the children will be inserted
* */
const Main = React.createClass({
  render() {
    return (
      <div>
        <h1>
          <Link to='/'>Race Tracker</Link>
        </h1>
        {React.cloneElement({...this.props}.children, {...this.props})}
      </div>
    )
  }
});

export default Main;
