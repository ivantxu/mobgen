import React from 'react';
import { Link } from 'react-router';
import Photo from './Photo';
import Results from './Results'

/*
* Race detail view with info about the race and a component that displays previous results
* */
const RaceDetail = React.createClass({
    render() {
        const { raceId } = this.props.params;
        const race = this.props.races[i];
        const i = this.props.races.findIndex((race) => race.id === raceId);

        return (
            <div className='single-photo row'>
                <Link to={'/'}>
                    <p>← Back to races</p>
                </Link>
                <h2 className='col-xs-12'>{this.props.races[i].name}</h2>
                <Photo {...this.props} showCaption={false} key={i} i={i} race={this.props.races[i]} />
                <div className='race-data'>
                    <p>{this.props.races[i].description}</p>
                    <p><b>Race length:</b> {this.props.races[i].length} meters</p>
                    <Results {...this.props} raceId={i}/>
                </div>
            </div>
        )
    }
});

export default RaceDetail;
