import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import RaceDetail from './RaceDetail';

/*
* Container to get user, races and result data into child components for the race detail view
* */
function mapStateToProps(state) {
    return {
        user: state.user,
        races: state.races,
        results: state.results
    }
}

function mapDispachToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const SingleRaceContainer = connect(mapStateToProps, mapDispachToProps)(RaceDetail);

export default SingleRaceContainer;
