import React from 'react';
import { Link } from 'react-router';

/*
* Component shared by the grid and detail view
* The caption with the name of the race can be hidden via props
* */
const Photo = React.createClass({
  render() {
    const { race } = this.props;
    return (
      <figure className='grid-figure col-sm-5'>
        <div className='grid-photo-wrap'>
          <Link to={`/view/${race.id}`}>
            <img src={race.photo} alt={race.name} className='grid-photo' />
          </Link>
        </div>
          { this.props.showCaption &&
          <figcaption>
            <p>{race.name}</p>
          </figcaption>}

      </figure>
    )
  }
});

export default Photo;
