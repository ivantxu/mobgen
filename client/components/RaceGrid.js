import React from 'react';
import Photo from './Photo';

/*
* Grid where all the races will be displayed
* */
const RaceGrid = React.createClass({
    render() {
        return (
            <div className='race-grid row'>
                {this.props.races.map((race, i) => <Photo {...this.props} showCaption="true" key={i} i={i} race={race} />)}
            </div>
        )
    }
});

export default RaceGrid;
