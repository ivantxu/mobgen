import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import Home from './Home';

/*
* Mapping only user and races because in this view there are no results to be shown
* */
function mapStateToProps(state) {
  return {
      user: state.user,
      races: state.races
  }
}

function mapDispachToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

const App = connect(mapStateToProps, mapDispachToProps)(Home);

export default App;
