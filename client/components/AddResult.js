import React from 'react';
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {Button, FormGroup, ControlLabel, FormControl, Modal} from 'react-bootstrap'

/*
*  Creates a custom field for Redux Form with validation
* */
const renderField = ({type, label, input, meta: {touched, error}}) => (
    <div className='input-row'>
        <FormGroup validationState={getValidationState(error)}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl type={type} { ...input } />
        </FormGroup>
        {touched && error &&
        <span className='error'>{error}</span>}
    </div>
);

/*
* Set the validation error to change React Bootstrap field color
* */
const getValidationState = (errors) => {
    return errors ? 'error' : null;
};

/*
* This component adds a button that triggers a React Bootstrap modal where you can add a new result
* */
let AddResult = React.createClass({
    /*
    * By default the Modal is hidden
    * */
    getInitialState() {
        return {showModal: false};
    },
    /*
    * Closes the add result modal
    * */
    close() {
        this.setState({showModal: false});
    },
    /*
    * Open the add result modal
    * */
    open() {
        this.setState({showModal: true});
    },
    render() {
        /*
        * Method to add a new result to the store and handle the validation of the fields
        * */
        const submit = (values , dispatch, props) => {
            const { year = '', time = '', position = ''} = values;
            let error = {};
            let isError = false;
            if (year.trim() === '') {
                error.year = 'Required';
                isError = true;
            }
            if (position.trim() === '') {
                error.position = 'Required';
                isError = true;
            }
            if (time.trim() === '') {
                error.time = 'Required';
                isError = true;
            }
            if (isError) {
                throw new SubmissionError(error)
            } else {
                dispatch(props.addResult(values, this.props.user.loggedUser, this.props.raceId));
                this.close();
            }

        };

        const {handleSubmit} = this.props;
        return (

            <div>

                <Button
                    bsStyle='primary'
                    bsSize='large'
                    onClick={this.open}
                >
                    Add new result
                </Button>

                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={handleSubmit(submit.bind(this))}>
                            <Field name='year' label='YEAR' component={renderField} type='number'/>
                            <Field name='position' label='POSITION' component={renderField} type='number'/>
                            <Field name='time' label='MARK' component={renderField} type='text'/>
                            <Button
                                bsStyle='primary'
                                bsSize='small'
                                type='submit'
                            >
                                SUBMIT
                            </Button>
                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
});

AddResult = reduxForm({
    form: 'addresult'
})(AddResult);

export default AddResult;
