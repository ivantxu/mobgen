function user(state = [], action) {
    switch(action.type) {
        case 'LOGIN_USER' :
            state.loggedUser = state.login;
            return state;
        default:
            return state;
    }
}

export default user;