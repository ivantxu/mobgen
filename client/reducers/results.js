function results(state = [], action) {
    switch(action.type) {
        case 'ADD_RESULT' :
            return {
                ...state,
                [action.user]: {
                    ...state[action.user],
                    [action.raceId]: {
                        ...state[action.user][action.raceId],
                        [action.result.year]: {
                            time: action.result.time,
                            position: action.result.position
                        }
                    }
                }
            };
        default:
            return state;
    }
}

export default results;