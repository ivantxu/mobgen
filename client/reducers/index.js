import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form} from 'redux-form';

import user from './user';
import races from './races';
import results from './results';

const rootReducer = combineReducers({ user, form, races, results, routing: routerReducer });

export default rootReducer;
