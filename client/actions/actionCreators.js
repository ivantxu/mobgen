// log user in

export function doLogin(login) {
  return {
    type: 'LOGIN_USER',
      login
  }
}

// Add a result to the store
export function addResult(result, user, raceId) {
  return {
    type: 'ADD_RESULT',
      result,
      user,
      raceId
  }
}