import React from 'react';

import { render } from 'react-dom';

// Import css
import css from './styles/style.styl';

// Import Components
import Main from './components/Main';
import App from './components/App';
import SingleRaceContainer from './components/SingleRaceContainer'

// import react router deps
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from './store';

/*
 * Before entering a race detail we check if the user is logged so we don't expose user data
 * If the user is not logged we send it to the Home so he can log in
 * */
const authedComponent = () => {
    setTimeout(function () {authStatus('/', false)}, 100);
};

const authStatus = (route) => {
    let isLogged = typeof (store.getState().user.loggedUser) !== 'undefined';

    if (!isLogged) {
        browserHistory.push(route);
    }
};

const router = (
  <Provider store={store}>
    <Router history={history}>
      <Route path='/' component={Main}>
        <IndexRoute component={App}/>
          <Route path='/view/:raceId' component={SingleRaceContainer} onEnter={authedComponent} />
      </Route>
    </Router>
  </Provider>
);

render(router, document.getElementById('root'));
