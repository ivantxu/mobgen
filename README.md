# Code test

Micro web app built for the Mobgen code test. The technologies used were React + Redux and Redux Form with React Bootstrap for the user interface elements.

As I enjoy running and in Coruña we have a year long running competition called "Coruña Corre", composed of the same 7 races that repeat each year. I decided to make a simple web app where a runner could keep track of his evolution.

There is a login form that implements validation as well as login/password verification.

Once logged in a grid with the posters from the races and its names is presented.

When clicked the user will be redirected to the detail of the races where he can view his results and add new results. Edition of previous entered results is possible entering a new result for a previously entered year.

All the dummy data is loaded from the files in the data folder.

## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:7770> in your browser.

## Production Build

Run `npm run build` to create a distro folder and a bundle.js file.
